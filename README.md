
## Structure

If any of the following are applicable to your project, then the directory structure should follow industry best practices by being named the following.

```
your_Laravel_project/
vendor/
```


## Install

Via Composer

``` bash0
$ composer require yogesh-laravel/stripe

$ composer dump-autoload

Add Environment Key STRIPE_SECRET_KEY in .env
Add Environment Key STRIPE_CURRENCY in .env

```

## Usage

``` php laravel 

use yogeshlaravel\stripe\StripeHelper;

```
## Plan API Use

``` php laravel 
    StripeHelper::createPlan($amount,$product_name);
    StripeHelper::updatePlan($stripe_price_id,$order_id);
    StripeHelper::deletePlan($stripe_price_id);
```
## Customer API Use

``` php laravel 
    StripeHelper::addCustomer($user);
    StripeHelper::updateCustomer($user);
    StripeHelper::getCustomer($stripe_customer_id);
```
## Card API Use

``` php laravel 
    StripeHelper::createCard($stripe_customer_id, $card_token);
    StripeHelper::allCards($stripe_customer_id,$limit);
    StripeHelper::deleteCard($stripe_customer_id, $cardId);
```
## Subscription API Use

``` php laravel 
    StripeHelper::createSubscription($stripe_customer_id, $stripe_price_id);
    StripeHelper::updateSubscription($stripe_subscription_id, $stripe_price_id);
    StripeHelper::cancelSubscription($user);
```
## Charge API Use

``` php laravel 
    StripeHelper::createDirectCharge($cardToken, $amount, $desc=null);
    StripeHelper::createCharge($stripe_customer_id, $cardId, $amount, $desc=null);
    StripeHelper::createRefund($charge_id);
```
## Price API Use

``` php laravel 
    StripeHelper::createPrice($amount,$product_name);
```
## Security

If you discover any security related issues, please email yrana8786@gmail.com instead of using the issue tracker.

## Credits

- [kumarashok30592@gmail.com]

