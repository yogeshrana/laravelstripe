<?php

namespace yogeshlaravel\stripe;

use Stripe\Charge;
use Stripe\Stripe;
use Stripe\Transfer;

class StripeHelper
{
    public static function createPlan($amount,$name)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
            $name = $name ?? $amount."_plan";
            $plan = $stripe->plans->create([
                'amount' => $amount,
                'currency' => env('STRIPE_CURRENCY'),
                'interval' => 'month',
                'product' => ['name' => $name],
            ]);

        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $plan;
    }

    public static function updatePlan($stripe_price_id,$order_id)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
            $plan = $stripe->plans->update(
                $stripe_price_id,
                ['metadata' => ['order_id' => $order_id]]
              );

        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $plan;
    }
 
    public static function deletePlan($stripe_price_id)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
            $plan = $stripe->plans->delete(
                $stripe_price_id,
                []
            );

        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $plan;
    }

    public static function addCustomer($user)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));

            $customer = $stripe->customers->create([
                'email' => $user->email ?? '',
                'name' => $user->name ?? '',
                'phone' => $user->phone ?? '',
            ]);

        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $customer;
    }

    public static function updateCustomer($user)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));

            $customer = $stripe->customers->update($user->stripe_customer_id ?? '', [
                'email' => $user->email ?? '',
                'name' => $user->name ?? '',
                'phone' => $user->phone_number ?? '',
            ]);

        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $customer;
    }

    public static function getCustomer($stripe_customer_id)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
            $customer = $stripe->customers->retrieve($stripe_customer_id ?? '');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $customer;
    }

    public static function createCard($stripe_customer_id, $card_token)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
            $card = $stripe->customers->createSource($stripe_customer_id, [
                'source' => $card_token
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $card;
    }

    public static function allCards($stripe_customer_id,$limit)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
            $cards = $stripe->customers->allSources($stripe_customer_id, ['object' => 'card', 'limit' => $limit]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $cards->data;
    }

    public static function deleteCard($stripe_customer_id, $cardId)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
            $cards = $stripe->customers->deleteSource($stripe_customer_id, $cardId, []);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return true;
    }

    public static function createDirectCharge($cardToken, $amount, $desc=null)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));

            $charge = $stripe->charges->create([
                'amount'        => $amount,
                'currency'      => env('STRIPE_CURRENCY'),
                'source'        => $cardToken,
                'description'   => $desc ?? 'Charges for Donation',
            ]);

        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return $charge;
    }

    public static function createCharge($stripe_customer_id, $cardId, $amount, $desc=null)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
            $customer = $stripe->customers->retrieve($stripe_customer_id);

            $charge = $stripe->charges->create([
                'amount'        => $amount,
                'currency'      => env('STRIPE_CURRENCY'),
                'customer'      => $customer,
                'source'        => $cardId,
                'description'   => $desc ?? 'Charges for land',
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return $charge;
    }

    public static function createRefund($charge_id)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
            $refund = $stripe->refunds->create([
                'charge' => $charge_id ?? '',
                'reason' => "requested_by_customer",
            ]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $refund;
    }

    public static function createPrice($amount,$product_name)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));

            $price = $stripe->prices->create([
                'unit_amount' => $amount ?? 0,
                'currency' => env('STRIPE_CURRENCY'),
                'recurring' => ['interval' => 'month'],
                'product_data' => ['name' => $product_name ?? ''],
            ]);

        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return $price;
    }
    
    public static function createSubscription($stripe_customer_id, $stripe_price_id)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
            $subscription = $stripe->subscriptions->create([
                'customer' => $stripe_customer_id,
                'items' => [
                    ['price' => $stripe_price_id],
                ],
            ]);
 
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return $subscription;
    }

    public static function updateSubscription($stripe_subscription_id, $stripe_price_id)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
            $oldSubscription = $stripe->subscriptions->retrieve($stripe_subscription_id);

            $subscription = $stripe->subscriptions->update($stripe_subscription_id, [
                'cancel_at_period_end' => false,
                'proration_behavior' => 'create_prorations',
                'items' => [
                    [
                        'id' => $oldSubscription->items->data[0]->id,
                        'price' => $stripe_price_id,
                    ],
                ],
            ]);
 
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        return $subscription;
    }

    public static function cancelSubscription($user)
    {
        try {
            $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET_KEY'));
            if ($user->stripe_subscription_id) {
                $cancel = $stripe->subscriptions->cancel($user->stripe_subscription_id, []);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $cancel;
    }

}
